package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password");
    }

    @Test(expected=ValidateFailedException.class)
    public void testPasswordValidationWhenInputLessThan8CharsPassword() throws ValidateFailedException {
        loginManager.register("Testuser1", "Passwor");
    }

    @Test(expected=ValidateFailedException.class)
    public void testPasswordValidationWhenInputAllLowerCasePassword() throws  ValidateFailedException {
        loginManager.register("Testuser1", "password");
    }

    @Test
    public void testLoginSuccess() throws UserNotFoundException, InvalidPasswordException {
        User user = loginManager.login("Testuser1", "Password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password"));
    }
    
    @Test
    public void testPasswordValidationSuccess() throws ValidateFailedException {
        loginManager.register("Testuser1", "Password");
    }
    

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws UserNotFoundException,InvalidPasswordException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws UserNotFoundException,InvalidPasswordException {
        User user = loginManager.login("iniad", "Password");
    }

}
